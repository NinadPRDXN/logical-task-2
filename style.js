var list = [];

function myFunction() {
    var fName = document.getElementById('first-name-input').value;
    var lName = document.getElementById('last-name-input').value;
    var address = document.getElementById('address-input').value;
    let txt = " ";

    if (document.getElementById("male").checked) {
        var gender = document.getElementById("male").value;
    }
    else {
        var gender = document.getElementById("female").value;
    }

    var letters = /^[A-Za-z]+$/;
    if (!letters.test(fName)) {
        alert('Firstname should be in alphabetical form and should not be blank');
    }
    else if (!letters.test(lName)) {
        alert('Lastname should be in alphabetical form and should not be blank');
    }
    else if (!document.getElementById("male").checked && !document.getElementById("female").checked) {
        alert('Please select the gender.');
    }
    else if (!document.getElementById("term-condition-input").checked) {
        alert('Please accept the Terms and Condition to proceed further.');
    }
    else {
        document.getElementById('table-body').innerHTML = ' ';
    
        list.push({'firstName': fName, 'lastName': lName, 'gender': gender, 'address': address});

        list.forEach(listFunction);

        function listFunction(data, index) {
            txt += '<ul><li>' + (index+1) + '</li><li>' + data.firstName + '</li><li>' + data.lastName + '</li><li class="' + data.gender + '">' + data.gender + '</li><li>' + data.address + '</li><li><button onclick="edit(' + index + ')">edit</button></li><li><button onclick="remove(' + index + ')">delete</button></li></ul>';
        }
        document.getElementById('table-body').innerHTML = txt;  

    }

}

function cancel() {
    document.getElementById('first-name-input').value = '';
    document.getElementById('last-name-input').value = '';
    document.getElementById('address-input').value = '';
    document.getElementById('male').checked = false;
    document.getElementById('female').checked = false;
    document.getElementById('term-condition-input').checked = false;
    document.getElementById('btn').innerHTML = '<button onclick="myFunction()">submit</button><button onclick="cancel()">cancel</button>';
}

function edit(index) {
    console.log(list);
    document.getElementById('first-name-input').value = list[index].firstName;
    document.getElementById('last-name-input').value = list[index].lastName;
    document.getElementById('address-input').value = list[index].address;
    if (list[index].gender == 'male') {
        document.getElementById('male').checked = true;
    }
    else {
        document.getElementById('female').checked = true;
    }
    document.getElementById('btn').innerHTML = '<button onclick="update(' + index + ')">edit</button><button onclick="cancel()">cancel</button>';
}

function update(index) {
    var fName = document.getElementById('first-name-input').value;
    var lName = document.getElementById('last-name-input').value;
    var address = document.getElementById('address-input').value;
    if (document.getElementById("male").checked) {
        var gender = document.getElementById("male").value;
    }
    else {
        var gender = document.getElementById("female").value;
    }

    list[index].firstName = fName;
    list[index].lastName = lName;
    list[index].gender = gender;
    list[index].address = address;

    document.getElementById('table-body').innerHTML = ' ';
    txt = ' ';
    list.forEach(listFunction);

    function listFunction(data, index) {
        txt += '<ul><li>' + (index+1) + '</li><li>' + data.firstName + '</li><li>' + data.lastName + '</li><li class="' + data.gender + '">' + data.gender + '</li><li>' + data.address + '</li><li><button onclick="myFunction(' + index + ')">edit</button></li><li><button onclick="remove(' + index + ')">delete</button></li></ul>';

        document.getElementById('table-body').innerHTML = txt;
    }

}

function remove(index) {
    list.splice(index, 1);
    txt = ' ';
    document.getElementById('table-body').innerHTML = ' ';
    list.forEach(listFunction);

    function listFunction(data, index) {
        txt += '<ul><li>' + (index+1) + '</li><li>' + data.firstName + '</li><li>' + data.lastName + '</li><li class="' + data.gender + '">' + data.gender + '</li><li>' + data.address + '</li><li><button onclick="myFunction(' + index + ')">edit</button></li><li><button onclick="remove(' + index + ')">delete</button></li></ul>';

        document.getElementById('table-body').innerHTML = txt;
    }

    document.getElementById('btn').innerHTML = '<button onclick="myFunction()">submit</button><button onclick="cancel()">cancel</button>';

    console.log(list);
}

function confirmation() {
    var userConfirmation = confirm("Are you sure that you have read all terms and conditions?");
    if (userConfirmation == true) {
        document.getElementById('term-condition-input').checked = true;
    }
    else {
        document.getElementById('term-condition-input').checked = false;
    }
}

function sort_by_name() {
    list.sort((a, b) => (a.firstName.toLowerCase() > b.firstName.toLowerCase()) ? 1 : -1);
    txt = ' ';
    document.getElementById('table-body').innerHTML = ' ';
    list.forEach(listFunction);

    function listFunction(data, index) {
        txt += '<ul><li>' + (index+1) + '</li><li>' + data.firstName + '</li><li>' + data.lastName + '</li><li class="' + data.gender + '">' + data.gender + '</li><li>' + data.address + '</li><li><button onclick="myFunction(' + index + ')">edit</button></li><li><button onclick="remove(' + index + ')">delete</button></li></ul>';

        document.getElementById('table-body').innerHTML = txt;
    }
}

function sort_by_lastname() {
    list.sort((a, b) => (a.lastName.toLowerCase() > b.lastName.toLowerCase()) ? 1 : -1);
    txt = ' ';
    document.getElementById('table-body').innerHTML = ' ';
    list.forEach(listFunction);

    function listFunction(data, index) {
        txt += '<ul><li>' + (index+1) + '</li><li>' + data.firstName + '</li><li>' + data.lastName + '</li><li class="' + data.gender + '">' + data.gender + '</li><li>' + data.address + '</li><li><button onclick="myFunction(' + index + ')">edit</button></li><li><button onclick="remove(' + index + ')">delete</button></li></ul>';

        document.getElementById('table-body').innerHTML = txt;
    }
}